# py_blog

## 使用Flask和SQLite3的轻型博客

--------------------------------------------------------------------------------

# 文件目录

**main.py**:<br>
flask网页程序入口

**blog.py**:<br>
博客引擎

--------------------------------------------------------------------------------

# API接口(正在编写中)

## /api/__init:<br>

### 说明:<br>

用于初始化博客

### 访问类型: **POST**

### 参数:<br>

#### username: 管理员用户名

#### password: 管理员密码

#### force(可选): 强制初始化

### 返回实例:<br>

```javascript
{
  "code": 0,
  "username": "admin",
  "password": "admin"
}
```

## /api/login:<br>

### 访问类型: **POST**

### 参数:<br>

#### username: 用户名

#### password: 密码

### 返回实例:<br>

```javascript
{
  "code": 0,
  "username": "admin",
  "key": "3eb30933042d804feec9429a08a5d09c"
}
```

## /api/article_list:<br>
