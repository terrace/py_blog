// ; //marked编译设置
// (function() {
//     marked.setOptions({
//         renderer: new marked.Renderer(),
//         gfm: true,
//         tables: true,
//         breaks: false,
//         pedantic: false,
//         sanitize: true,
//         smartLists: true,
//         smartypants: false,
//         highlight: function(code) {
//             return hljs.highlightAuto(code).value;
//         }
//     });
// })();

; //星星动画
(function() {
    if (document.body.offsetWidth < 980) {
        return false
    }
    var cvs = document.getElementById('cvs');
    var ctx = cvs.getContext('2d');

    var PI = Math.PI;
    var requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame

    /*初始化25个*/
    var box = [];
    for (var i = 0; i < 100; i++) {
        box.push({
            r: 0,
            R: 0,
            d: deg(Math.random() * 360),
            v: Math.random() * 20,
            b: 20,
            a: 1
        })
    }
    var w = cvs.width = window.screen.width;
    var h = cvs.height = window.screen.height;
    //重绘标志
    var flag = Math.sqrt(w * w + h * h)
    var ox = 0;
    var oy = h;

    document.body.addEventListener('mousemove', function(e) {
        ox = 50 * (w - e.clientX) / w;
        oy = h - 50 * (h - e.clientY) / h;
    })

    requestAnimationFrame(loop)

    function loop() {

        ctx.clearRect(0, 0, w, h);
        ctx.beginPath();
        for (var i in box) {
            if (box[i].R > flag) {
                box[i].R = 0;
                box[i].r = 0;
                box[i].d = deg(Math.random() * 90 + 270);
                box[i].v = Math.random() * 20;
                box[i].b = 20;
            }
            box[i].R += box[i].v;
            box[i].v += Math.random() * 0.05;
            box[i].r = 5 * box[i].R / flag;
            box[i].b += Math.random() * 0.05;
            fillStar(box[i]);
        }
        ctx.fillStyle = '#fff';
        ctx.shadowColor = '#ff0';
        ctx.fill();
        requestAnimationFrame(loop)

    }


    function deg(num) {
        return (PI * num) / 180
    }

    function fillStar(item) {
        var x = getPos(item.R, item.d).x;
        var y = getPos(item.R, item.d).y;
        ctx.arc(x, y, item.r, 0, deg(360));
        ctx.shadowBlur = item.b || 0;
        ctx.closePath();
    }

    /*建立圆坐标转换*/
    function getPos(R, d) {
        return {
            x: R * Math.cos(d) + ox,
            y: R * Math.sin(d) + oy,
        }
    }
})();

; //头信息
(function() {
    Vue.component('m-hd', {
        template: '#m_hd',
    })
})();


; //导航栏
(function() {
    Vue.component('m-nav', {
        template: '#m_nav',
        data: function() {
            return {
                nAtv: 0,
                list: [
                    ['&#xe62b;', '首页'],
                    ['&#xe661;', '友链'],
                ]
            }
        },
        methods: {
            eItemClick: function(i) {
                this.nAtv = i;
            }
        }
    })
})();

; //文章格子
(function() {
    Vue.component('m-cell-article', {
        template: '#m_cell_article'
    })
})();

; //app
(function() {
    new Vue({
        el: '#app'
    })
})();