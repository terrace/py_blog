; //marked编译设置
(function() {
    marked.setOptions({
        renderer: new marked.Renderer(),
        gfm: true,
        tables: true,
        breaks: false,
        pedantic: false,
        sanitize: true,
        smartLists: true,
        smartypants: false,
        highlight: function(code) {
            return hljs.highlightAuto(code).value;
        }
    });
})();

; //列表页
(function() {
    Vue.component('list-article', {
        template: '#list_article',
        data: function() {
            return {
                title: '列表'
            }
        }
    })
})();

; //添加友链页
(function() {
    Vue.component('add-link', {
        template: '#add_link',
    })
})();

; //账户管理页
(function() {
    Vue.component('set-user', {
        template: '#set_user',
        data: function() {
            return {
                img: 'http://imgsrc.baidu.com/forum/w=580/sign=54cd042fd5c451daf6f60ce386fd52a5/25af400ed9f9d72a2f418c6cdc2a2834359bbbfa.jpg',
            }
        },
        computed: {
            hdImg: function() {
                return this.img
            }
        },
        methods: {
            eChooseFile: function(e) {
                var vm = this;
                var _this = e.currentTarget;
                var fileReader = new FileReader();
                var file = _this.files[0];
                fileReader.readAsDataURL(file);
                fileReader.onload = function(e) {
                    vm.img = this.result;
                }
            }
        }
    })
})();

; //编辑文章页
(function() {
    Vue.component('write-article', {
        template: '#write_article',
        data: function() {
            return {
                title: '',
            }
        }
    })
})();

; //左侧导航
(function() {
    Vue.component('lt-nav', {
        template: '#lt_nav',
        data: function() {
            return {
                isAtv: 0,
                isShow: 0,
                aNavItems: [{
                    icon: '&#xe6b6;',
                    title: '文章列表',
                }, {
                    icon: '&#xe617;',
                    title: '写文章',
                }, {
                    icon: '&#xe647;',
                    title: '外链列表',
                }, {
                    icon: '&#xe661;',
                    title: '添加外链',
                }, {
                    icon: '&#xe75c;',
                    title: '账户管理',
                }, ]
            }
        },
        methods: {
            eItemClick: function(i) {
                this.isAtv = i;
                this.isShow=!this.isShow
                this.$emit('ltnavclick', i)
            }
        }
    })
})();

; //分页
(function() {
    Vue.component('m-paging', {
        template: '#pageIng',
        data: function() {
            return {
                isPage: 1,
                total: 10
            }
        },
        computed: {
            aPage: function() {
                var a = [];
                if (this.total <= 5) {
                    for (var i = 1; i <= this.total; i++) {
                        a.push(i)
                    }
                    return a;
                }
                if (this.total > 5 && this.isPage < 4) {
                    return [1, 2, 3, 4, 5]
                }
                if (this.total > 5 && this.isPage > this.total - 2) {
                    for (var i = this.total - 4; i <= this.total; i++) {
                        a.push(i);
                    }
                    return a
                }
                return [this.isPage - 2, this.isPage - 1, this.isPage, this.isPage + 1, this.isPage + 2]
            },
        },
        methods: {
            ePreClick: function() {
                if (this.isPage == 1) {
                    return false
                }
                this.isPage--;
            },
            eItemClick: function(i) {
                if (this.isPage == i) {
                    return false
                }
                this.isPage = i;
            },
            eNextClick: function() {
                if (this.isPage == this.total) {
                    return false
                }
                this.isPage++
            }
        }
    })
})();

; //编辑器
(function() {
    Vue.component('editer', {
        template: '#articleEditer',
        data: function() {
            return {
                title: '',
                tag: '',
                summary: '',
                content: '',
                nAtvId: 0,
                aNavItems: [{
                    title: '编辑',
                    icon: '&#xe617;',
                    isAtv: 1
                }, {
                    title: '可视编辑',
                    icon: '&#xe697;',
                    isAtv: 0
                }, {
                    title: '预览',
                    icon: '&#xe6b6;',
                    isAtv: 0
                }],
                sScreenStatus: '&#xe643;',
                isScreen: 0,
                aEditerModule: [1, 0, 0],
                isDialog: 0
            }
        },
        computed: {
            preViewContent: function() {
                return marked(this.content)
            }
        },
        methods: {
            eNavItemClick: function(i) {
                this.aNavItems[this.nAtvId].isAtv = 0;
                this.aEditerModule[this.nAtvId] = 0;
                this.aNavItems[i].isAtv = 1;
                this.aEditerModule[i] = 1;
                this.nAtvId = i;
            },
            eScreenTabClick: function() {
                this.isScreen = !this.isScreen;
                this.sScreenStatus = this.isScreen ? '&#xe620;' : '&#xe643;';
            },
            eEditerKeyDown: function(e) {
                var self = e.currentTarget;
                if (e.keyCode == 9) {
                    e.preventDefault();
                    this._insertText(self, '    ');
                }
                if (e.keyCode == 13) {
                    this._insertText(self, '  ');
                }
                if (e.keyCode == 221 && e.ctrlKey) {
                    this._tabText(self, ']');
                }
                if (e.keyCode == 219 && e.ctrlKey) {
                    this._tabText(self, '[');
                }
            },
            _insertText: function(self, insert) {
                var start = self.selectionStart;
                var end = self.selectionEnd;
                var value = self.value;
                this.content = self.value = value.substr(0, start) + insert + value.substr(start);
                self.selectionStart = self.selectionEnd = start + insert.length;
            },
            _tabText: function(self, flag) {
                var start = self.selectionStart;
                var end = self.selectionEnd;
                var value = self.value;
                var select = value.slice(start, end - 1);
                var target;
                if (flag == ']') {
                    target = select.replace(/\n/g, '\n    ');
                    if (start - 1 > 0 && /\n/.test(value[start - 1])) {
                        target = '    ' + target;
                    }
                }
                if (flag == '[') {
                    target = select.replace(/\n    /g, '\n');
                    if (start - 1 > 0 && /\n/.test(value[start - 1])) {
                        target = target.replace(/^    /, '');
                    }
                }
                this.content = self.value = value.replace(select, target);
                self.selectionEnd = start + target.length;
                self.selectionStart = start;
            }
        }
    })
})();

; //alert
(function() {
    Vue.component('m-dialog', {
        template: '#alert'
    })
})();

; //app
(function() {
    new Vue({
        el: '#app',
        data: {
            isCpmt: 0,
            cpmt: ['list-article', 'write-article', 'list-article', 'add-link', 'set-user'],
        },
        computed: {
            getCpmt: function() {
                return this.cpmt[this.isCpmt];
            }
        },
        methods: {
            eComponentChange: function(i) {
                this.isCpmt = i;
            }
        }
    })
})();